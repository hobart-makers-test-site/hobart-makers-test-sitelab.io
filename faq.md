---
layout: default
title: Frequently Asked Questions
permalink: /faq/
---

# {{ page.title }}


### :thought_balloon: Why do you exist?

Our goal is to create a large and supportive maker community. We host regular open nights, show and tells, and crafternoons to make this happen. One of our sub-goals is to run more workshops and demos.

We want to promote and encourage people to make, repair and repurpose things using both old and new technologies. Most of the founders have a background in software and electronics, but are interested in a much broader range of crafts and skills. We want to promote *all* crafts and skills that are interesting to our members.

Come and meet us and you'll get the idea! :slight_smile:

### :mag: What about the other maker groups?

Hobart Makers was started by people that are (or were) members and committee-members of other maker and tech groups.

These existing groups include [Hobart Hackerspace](http://www.hobarthackerspace.org.au), [REAST - Radio and Electronics Association of Southern Tasmania](https://www.reast.asn.au/), the [Orielton Playgroup](https://www.facebook.com/Orielton-Play-Group-1568426043486728/), [TasLUG](https://taslug.org.au), and others. We love that these groups exist, and we hope they continue to. We'd like to complement these groups, not replace them.

We think that Hobart Makers has different goals to the existing groups. We want to offer things like low-cost membership, more training workshops, a wider range of maker equipment and tools, an inclusive, friendly and supportive community, central location, and lots more regular activities to keep people engaged.

Hobart Makers was dreamed up in late Jan 2017, and incorporated as a not-for-profit organisation on the 2nd Feb 2017. So, we're still young and we have some catching up to do.

We want to foster a connection between ourselves and other maker groups out there. We freely offer space to meet to HoGS (Hobart Games Society) and TasLUG when needed, and also host weekly events for Tas Brick Enthusiasts, and the Rostrum group - so that the space is being used to its best.

### :label: Why Hobart Makers, and not Hobart Makerspace?

We think the people are the focus, and the makerspace is secondary.

With our low-cost membership (including a $0 option), we are at risk of one day not being able to pay the bills. If that happens, we like to think that we can continue to operate as a roving maker group, using the free and low-cost spaces around Hobart for workshops and meetups.

Of course it's preferable to have a makerspace, but we try to remain un-attached to any particular space (and uncommitted to any particular expenses!).

For our first 18 months, we were fortunate to have had the support of [Kickstart Arts](http://www.kickstartargs.org) who offered us a cleaning-for-rent deal.

Since September 2018 - we've had the generous support of [Enterprize Hobart](https://www.enterprize.space/). We have access to some dedicated area for our machines, tools, and workbenches, and to a lot of common area for folks that need less permanent space for their projects.

### :dollar: What does membership cost?

Our membership fees are very flexible. We hope for $20/month, so that we can grow faster, but you can pay whatever you think membership is worth, or whatever you can afford. If you have zero money, you are still welcome :slight_smile:

### :thumbsup: Do you have "Working with Vulnerable People" cards?

Yes. Most of our committee members have their working with vulnerable people cards, the most recent couple of committee members to join will get theirs soon.

We also encourage anyone that wants to run a workshop to obtain a card too.

