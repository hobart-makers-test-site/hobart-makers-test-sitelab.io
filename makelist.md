---
layout: default
title: Making Wishlist
permalink: /makelist/
---

<h3>Making Wishlist</h3>

<p>This is a list of projects and things that you can volunteer to help out with.</p>

<p>If you want to join in, please <a href="/contact/">get in touch</a></p>

<table class="table">
 <thead>
   <tr>
   <th>Item/Project</th>
   <th>Notes</th>
   </tr>
 </thead>

<tbody>
{% for item in site.data.makelist %}
   <tr>
   <td>{{ item.item }}</td>
   <td>{{ item.notes }}</td>
   </tr>
{% endfor %}
</tbody>

</table>
